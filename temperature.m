function T = temperature(H)
    T = 0;
    if (T > 0 && T <= 11000)
        T = 288.150 - 0.0065 .* H;
    elseif (T <= 20000)
        T = 216.650;
    elseif (T <= 32000)
        T = 216.650 + 0.001 .* (H - 20000);
    elseif (T <= 47000)
        T = 228.650 + 0.028 .* (H - 32000);
    end
end

