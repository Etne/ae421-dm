clear all;
close all;
clc;

format short;

%% Import constants
load("constants.mat");

%% Matrix A
beef = (q_bar * S) / (m * U0);

Xu = -beef * (2 * CD_0 +  CD_u);
Xw = beef * (CL_0 - ((2 / (pi * e * AR)) * CL_0 * CL_alpha));

Zu = -beef * (2*CL_0 + (M^2 / (1 - M^2)) * CL_0);
Zw = -beef * (CD_0 + CL_alpha);
Zq = beef * (c_bar/2) * CL_q;

Mu = ((q_bar * S * c_bar) / (I_yy * U0)) * CM_u;
Mw = ((q_bar * S * c_bar) / (I_yy * U0)) * CM_alpha;
Mw_d = ((q_bar * S * c_bar^2) / (2 * I_yy * U0^2)) * CM_alpha_d;
Mq = ((q_bar * S * c_bar^2) / (2 * I_yy * U0)) * CM_q;

A = [[Xu, Xw, 0, -g*cosd(theta_0)]; 
    [Zu, Zw, U0, -g*sind(theta_0)];
    [Mu + Mw_d*Zu, Mw + Mw_d*Zw, Mq + U0*Mw_d, -Mw_d*g*sind(theta_0)];
    [0, 0, 1, 0]];

disp("Question 2 -------------------");
disp("Matrix A");
disp(A)

%% Matrix B
X_delta_e = beef * CD_delta_e;
X_delta_T = beef * CD_delta_T;

Z_delta_e = beef * CL_delta_e;
Z_delta_T = beef * CL_delta_T;

M_delta_e = ((q_bar * S * c_bar) / (I_yy * U0)) * CM_delta_e;
M_delta_T = ((q_bar * S * c_bar) / (I_yy * U0)) * CM_delta_T;

B = [[X_delta_e, X_delta_T];
    [Z_delta_e, Z_delta_T];
    [M_delta_e + Mw_d * Z_delta_e, M_delta_T + Mw_d * Z_delta_T];
    [0, 0]];

%% Charasteristic equations
[size_A, ~] = size(A); 
coeff_poly = poly(A);
syms x;
polynomial = vpa(det(A - x * eye(size_A)), size_A);

disp("");
disp("Question 3 -------------------");
disp("Characteristic equation:");
disp(polynomial);

%% Eigenvalues
eigenvalues = eig(A);
[eigvec, eigval] = eig(A);

disp("Question 4 -------------------");
disp("Eigenvalues:");
disp(eigenvalues);

ph_id = 3;
sp_id = 1;

ph = eigenvalues(ph_id);
sp = eigenvalues(sp_id);

% Recall: 
% - Eigenvalues of A define stability of x_d = A * x
% - A is 4x4 so A has 4 eigenvalues
% - Stable if eigenvalues all have negative real part
% - The eigenvalues couple that have the smallest abs real part are the phugoid

%% Oscillation modes
% Short period mode
eta_sp = sqrt(1 / (1 + (imag(sp) / real(sp))^2));
omega_sp = -real(sp) / eta_sp;

% Phugoid mode
A_ph = [Xu -g; -Zu/U0 0]; % Matrix for the phugoid
A_ph_poly = poly(A_ph); % This define the polynome of the phugoid mode

eta_ph = sqrt(1 / (1 + (imag(ph) / real(ph))^2));
omega_ph = -real(ph) / eta_sp;

disp("");
disp("Question 5 -------------------");
disp("Short period mode:");
disp(['  Natural frenquency: ', num2str(omega_sp), ' rad/s; Damping factor: ', num2str(eta_sp)]); 
disp("Phugoid mode:");
disp(['  Natural frenquency: ', num2str(omega_ph), ' rad/s; Damping factor: ', num2str(eta_ph)]);

%% Curves
% Short period mode
t_S = 0:0.01:10;
X_S = eigvec(:, sp_id) * exp(eigval(sp_id, sp_id) * t_S);

figure;
hold on;
plot(t_S, X_S);
title('Short periode mode')
legend('Axial velocity', 'Angle of attack', 'Pitch angle', 'Pitch rate')
xlabel('Time (sec)')
ylabel('State variable');

% Phugoid mode
t_P = 0:0.1:500;
X_P = eigvec(:, ph_id) * exp(eigval(ph_id, ph_id) * t_P);

figure()
hold on;
plot(t_P, X_P);
title('Phugo�de mode')
legend('Axial velocity', 'Angle of attack', 'Pitch angle', 'Pitch rate')
xlabel('Time (sec)')
ylabel('State variable');

%% Transfert functions
